﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WzmocnienieHeal : Przedmiot
{

    protected override void kolizja()
    {
        Gracz gracz = Gracz.getGracz();
        gracz.dealDemage(-20f);
        Debug.Log("Healed XD");
        StartCoroutine(ResetBuff());
        gameObject.transform.position = new Vector3(0, 0, -55);
    }
    protected override IEnumerator ResetBuff()
    {
        yield return new WaitForSeconds(1); //wait 10 seconds
        Destroy(gameObject);
    }
}
