﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WzmocnienieDMG : Przedmiot
{

    // Update is called once per frame
    void Update()
    {
        
    }
    protected override void kolizja()
    {
        Gracz gracz = Gracz.getGracz();
        gracz.setDMG(gracz.getInitDMG()*2);
        Debug.Log("DEMAGE buff given XD");
        StartCoroutine(ResetBuff());
        gameObject.transform.position = new Vector3(0, 0, -55);
    }
    protected override IEnumerator ResetBuff()
    {
        Gracz gracz = Gracz.getGracz();
        yield return new WaitForSeconds(10); //wait 10 seconds
        gracz.setDMG(gracz.getInitDMG());
        Debug.Log("DEMAGE buff lost XD");
        Destroy(gameObject);
    }
}
