﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drzewo : Przedmiot
{
    public Sprite broken;
    // Update is called once per frame
    void Update()
    {

    }
    protected override void kolizja()
    {
       
        Debug.Log("drzewo");
        StartCoroutine(ResetBuff());
        gameObject.GetComponent<SpriteRenderer>().sprite = broken;
        gameObject.GetComponent<SpriteRenderer>().sortingLayerName = "UnderPlayer";
    }
    protected override IEnumerator ResetBuff()
    {
        yield return new WaitForSeconds(2); //wait 10 seconds
    }
}
