﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WzmocnienieSzybkosci : Przedmiot
{

    protected override void kolizja()
    {
        Gracz gracz = Gracz.getGracz();

        gracz.setSpeed(12f);
        Debug.Log("Buff given XD");
        StartCoroutine(ResetBuff());
        gameObject.transform.position = new Vector3(0, 0, -55);
    }
    protected override IEnumerator ResetBuff()
    {
        Gracz gracz = Gracz.getGracz();

        yield return new WaitForSeconds(10); //wait 10 seconds
        gracz.setSpeed(gracz.getInitSpeed());
        Debug.Log("Buff lost XD");
        Destroy(gameObject);
    }
}
