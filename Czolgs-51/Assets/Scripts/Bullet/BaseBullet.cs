﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBullet : IBullet
{
    private GameObject start; //lufa 
    private GameObject prefab;
    public BaseBullet()
    {
        prefab = (GameObject)Resources.Load("Prefabs/BaseBullet");
    }

    void IBullet.draw(GameObject go, string sender)
    {
        if (start == null)
            start = go;
        if (sender.Equals("Player"))
        {
            start = GameObject.FindGameObjectWithTag("CannonBarrel");
            GameObject b = GameObject.Instantiate(prefab, start.transform.position, start.transform.rotation);
            b.GetComponent<Rigidbody2D>().AddForce(start.transform.up * 20f, ForceMode2D.Impulse);
        }
        else if (sender.Equals("Enemy"))
        {
            start = go;
            GameObject b = GameObject.Instantiate(prefab, start.transform.position, start.transform.rotation);
            b.GetComponent<Rigidbody2D>().AddForce(start.transform.up * 20f, ForceMode2D.Impulse);
        }
    }
}
