﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollision : MonoBehaviour
{
    Gracz gracz;
    void Start()
    {
        gracz = Gracz.getGracz();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject explosion = GameObject.Instantiate((GameObject)Resources.Load("Prefabs/Explosion")
             ,gameObject.transform.position
            , Quaternion.identity);
        Destroy(explosion,1f);
         if (collision.gameObject.tag.CompareTo("PlayerTag") == 0)
         {
            gracz.dealDemage(gracz.getDiff()*10+10);
             Debug.Log("Player HP:"+ gracz.getHP());
         }
        if (collision.gameObject.tag.CompareTo("Enemy") == 0)
        {
            if (collision.gameObject.GetComponent<Typ>().getTyp() is PrzeciwnikEasy)
            {
                if (!collision.gameObject.GetComponent<PrzeciwnikEasy>().dealDamage(gracz.getDamage()))
                {
                    Destroy(collision.gameObject);


                }
                    
                Debug.Log("Enemy HP:" + ((PrzeciwnikEasy)(collision.gameObject.GetComponent<Typ>().getTyp())).getHP());
                
            }
            if (collision.gameObject.GetComponent<Typ>().getTyp() is PrzeciwnikHard)
            {
                if (!collision.gameObject.GetComponent<PrzeciwnikHard>().dealDamage(gracz.getDamage()))
                    Destroy(collision.gameObject);
                Debug.Log("Enemy HP:" + ((PrzeciwnikHard)(collision.gameObject.GetComponent<Typ>().getTyp())).getHP());
            }
            if (collision.gameObject.GetComponent<Typ>().getTyp() is PrzeciwnikMedium)
            {
                if (!collision.gameObject.GetComponent<PrzeciwnikMedium>().dealDamage(gracz.getDamage()))
                    Destroy(collision.gameObject);
                Debug.Log("Enemy HP:" + ((PrzeciwnikMedium)(collision.gameObject.GetComponent<Typ>().getTyp())).getHP());
            }


        }
        Destroy(gameObject);
    }
}
