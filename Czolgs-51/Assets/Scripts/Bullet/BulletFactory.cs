﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFactory : MonoBehaviour
{

    Gracz gracz;
    // Start is called before the first frame update
    void Start()
    {
        gracz = Gracz.getGracz();
    }

    public static IBullet getBullet(int damage)
    {
        IBullet bullet;
        if (damage <15)
        {
            bullet = new BaseBullet();
            return bullet;
        }
        else if(damage<25)
        {
            bullet = new MediumBullet();
            return bullet;
        }
        else
        {
            bullet = new StrongBullet();
            return bullet; 
        }


    }

    public static void Shot(GameObject game)
    {
        int diff = Gracz.getGracz().getDiff();

        if (diff==0)
        {
            getBullet(10).draw(game, "Enemy");
            return;
        }
        else if (diff == 1)
        {
            getBullet(20).draw(game, "Enemy");
            return;
        }
        else
        {
            getBullet(30).draw(game, "Enemy");
            return;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Fire XD");
            getBullet((int)gracz.getDamage()).draw(gameObject,"Player");
        }    
    }
}
