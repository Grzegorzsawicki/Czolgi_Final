﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBullet
{
    void draw(GameObject go,string sender);
}
