﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float MaxSpeed;
    public GameObject cannonObject;
    Rigidbody2D rb;
    Camera cam;
    Vector2 mausePos, movement;
    Rigidbody2D rbCannon;
    

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        cannonObject = Instantiate((GameObject)Resources
            .Load("Prefabs/" + Gracz.getGracz().getCannonType())
            , gameObject.transform.position, gameObject.transform.rotation);
        rbCannon = cannonObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        movement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxisRaw("Vertical"));
    }

    void FixedUpdate()
    {
        MoveCharacter(movement);
    }

    private void MoveCharacter(Vector2 direction)
    {
        rb.MovePosition((Vector2)transform.position + (direction * MaxSpeed * Time.deltaTime));
    }
}
