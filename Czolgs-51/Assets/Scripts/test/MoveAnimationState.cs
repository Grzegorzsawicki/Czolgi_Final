﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAnimationState : IState
{

    public IState DoState(PlayerMovement player)
    {
        MoveAnimation(player);
        //Debug.Log("XDXD"+(player.previus.x + player.transform.position.x));
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
        {

            return player.moveAnimationState;
        }
        else
        {
            return player.idleState;
        }
    }

    private void MoveAnimation(PlayerMovement player)
    {
        if (player.i == true)
        {
            player.k1.GetComponent<SpriteRenderer>().sprite = player.state1.GetComponent<SpriteRenderer>().sprite;
            player.k2.GetComponent<SpriteRenderer>().sprite = player.state1.GetComponent<SpriteRenderer>().sprite;

        }
        else
        {
            player.k1.GetComponent<SpriteRenderer>().sprite = player.state2.GetComponent<SpriteRenderer>().sprite;
            player.k2.GetComponent<SpriteRenderer>().sprite = player.state2.GetComponent<SpriteRenderer>().sprite;
        }
        player.i = !player.i;
    }
}
