﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleAnimationState : IState
{
    
    public IState DoState(PlayerMovement player)
    {
        

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
        {
            return player.moveAnimationState;
        }
        else
        {
            return player.idleState;

        }

    }
}
