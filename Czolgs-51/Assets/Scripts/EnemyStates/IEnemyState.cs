﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 public interface IEnemyState
{
    IEnemyState DoState(PrzeciwnikEasy enemy);
    IEnemyState DoState(PrzeciwnikMedium enemy);
    IEnemyState DoState(PrzeciwnikHard enemy);


}
