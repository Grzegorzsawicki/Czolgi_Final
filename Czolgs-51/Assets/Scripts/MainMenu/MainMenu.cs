﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private static int currentCannon;
    private static int currentHull;
    private static int currentDifficulty;
    private static string name;

    public void Awake()
    {
        if (Gracz.getGracz() != null)
            SceneManager.LoadScene(1);
    }
    public void ReadName(string s)
    {
        name = s;
        Debug.Log(name);

    }
    private void SelectCannon(int index)
    {
        for(int i =0; i<transform.childCount;i++)
        {
            transform.GetChild(i).gameObject.SetActive(i==index);
        }
    }
    public void ChangeCannon(int changed)
    {
        currentCannon += changed;
        currentCannon %= transform.childCount;
        if (currentCannon < 0) currentCannon = transform.childCount - 1;
        SelectCannon(currentCannon);
    }
    private void SelectHull(int index)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(i == index);
        }
    }
    public void ChangeHull(int changed)
    {
        currentHull += changed;
        currentHull %= transform.childCount;
        if (currentHull < 0) currentHull = transform.childCount - 1;
        SelectHull(currentHull);

    }
    private void SelectDifficulty(int index)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(i == index);
        }
    }
    public void ChangeDifficulty(int changed)
    {
        currentDifficulty += changed;
        currentDifficulty %= transform.childCount;
        if (currentDifficulty < 0) currentDifficulty = transform.childCount - 1;
        SelectDifficulty(currentDifficulty);
    }
    // Start is called before the first frame update
    void Start()
    {
        currentCannon = 0;
        currentHull = 0;
        currentDifficulty = 0;
        name = "";
    }

// Update is called once per frame
    void Update()
      {
      }

    public void NewGame()
    {
        if (name.Length < 2) name = "PLAYER";
        string hull = "Player" + (currentHull+1).ToString();
        string cannon = "Cannon" + (currentCannon+1).ToString();
        Debug.Log(name);
        Debug.Log(currentCannon);
        Gracz.NewBudowniczyGracza()
            .ustawCannon(cannon)
            .ustawHull(hull)
            .ustawNazwe(name.ToUpper())
            .ustawTrudnosc(currentDifficulty).build();
        SceneManager.LoadScene(1);
    }
}
