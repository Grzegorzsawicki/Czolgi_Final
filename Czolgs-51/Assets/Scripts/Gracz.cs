﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Gracz 
{
    private static Gracz Instance = null;
    private string playerName, cannonType, hullType;
    private float color
        , hp
        , speed
        , damage;
    Image hp_bar;
    private float initHP, initSpeed, initDMG;
    private int difficulty;
    private GameObject graczObject;
    public GameObject getGraczObject()
    {
        return graczObject;
    }
    public int getDiff()
    {
        return Instance.difficulty;
    }
    public static BudowniczyGracza NewBudowniczyGracza()
    {
        if (Instance != null)
            return null;
        return new BudowniczyGracza();
    }
    public static Gracz getGracz()
    {
        return Instance;
    }
    public void upgrade(int dmg)
    {
        initDMG = initDMG + dmg;
        damage += dmg;
    }
    private Gracz(BudowniczyGracza budowniczyGracza)
    {
        difficulty = budowniczyGracza._difficulty;
        hullType = budowniczyGracza._hullType;
        cannonType = budowniczyGracza._cannonType;
        playerName = budowniczyGracza._playerName;
        this.hp = 100f;
        Instance = this;
    }
    public void StartGame()
    {
        graczObject = Object.Instantiate((GameObject)Resources
            .Load("Prefabs/" + hullType)
            , new Vector3(0, 0, 0)
            , Quaternion.identity);

        hp_bar = GameObject.Find("hp_status").GetComponent<Image>();
        if (difficulty == 0)
        {
            this.speed = 14f;
            this.hp = 100f;
            this.damage = 50f;
        }
        else if (difficulty == 1)
        {
            this.speed = 10f;
            this.hp = 100f;
            this.damage = 20f;
        }
        else
        {
            this.speed = 6f;
            this.hp = 100f;
            this.damage = 10f;
        }
        initHP = this.hp;
        initSpeed = this.speed;
        initDMG = this.damage;
    }
    public float getInitSpeed()
    {
        return initSpeed;
    }
    public float getInitDMG()
    {
        return initDMG;
    }
    public string getName()
    {
        return playerName;
    }
    public string getCannonType()
    {
        return cannonType;
    }
    public float getColor()
    {
        return color;
    }
    public float getHP()
    {
        return hp;
    }
    public float getSpeed()
    {
        return speed;
    }
    public void setSpeed(float speed)
    {
        this.speed = speed;
    }
    public float getDamage()
    {
        return damage;
    }
    public void setDMG(float damage)
    {
        this.damage = damage;
    }
    public void dealDemage(float x)
    {
        this.hp -= x;
        hp_bar.fillAmount = this.hp / initHP;
        //if (hp < 0) gameObject.SetActive(false);
    }

    public sealed class BudowniczyGracza : IBudowniczyGracza
    {
        internal string _playerName, _cannonType, _hullType;
        internal float _color;
        internal int _difficulty;
        public Gracz build()
        {
            return new Gracz(this);
        }
        internal BudowniczyGracza() { }
        public IBudowniczyGracza ustawNazwe(string nazwa)
        {
            _playerName = nazwa;
            return this;
        }
        public IBudowniczyGracza ustawCannon(string cannonType)
        {
            _cannonType = cannonType;
            return this;

        }
        public IBudowniczyGracza ustawHull(string hullType)
        {
            _hullType = hullType;
            return this;
        }

        public IBudowniczyGracza ustawTrudnosc(int x)
        {
            _difficulty = x;
            return this;

        }
    }

}
