﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBudowniczyGracza
{
    Gracz build();
    IBudowniczyGracza ustawNazwe(string nazwa);
    IBudowniczyGracza ustawCannon(string cannonType);
    IBudowniczyGracza ustawHull(string hullType);
    IBudowniczyGracza ustawTrudnosc(int x);

}
