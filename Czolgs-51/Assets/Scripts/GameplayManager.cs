﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
    public static List<GameObject> gameObjectsList;
    public GameObject prefab;
    public static bool gameStated;
    private Gracz gracz;
    Dictionary<GameObject, TextMesh> listaPrzecinikow = new Dictionary<GameObject, TextMesh>();
    // public CameraMovement cameraMovement;
    //cameraMovement = FindObjectOfType<CameraMovement>();
    //cameraMovement.Destroy();
    private float nextActionTime = 0.0f;
    PrzeciwnikFactory przeciwnikFactory = new PrzeciwnikFactory();
    public float period = 1f;
    public int stage = 0;
    public int money = 0;
    public int static_money = 10;
    public TextMeshProUGUI stageText;
    public TextMeshProUGUI money_ui;
    public GameObject panel;
    GameObject button_upgrade;
    void Awake()
    {
        money_ui = GameObject.Find("Money").GetComponent<TextMeshProUGUI>();
        gracz = Gracz.getGracz();
        if (gracz == null)
        {
            SceneManager.LoadScene(0);
            return;
        }
        button_upgrade = GameObject.Find("Upgrade_Button").gameObject;
        stage = 0;
        gracz?.StartGame();
        foreach (GameObject x in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Debug.Log("Enemy destroyed");
            Destroy(x);
        }
        StartStage(++stage);
        gameStated = true;
        gracz.getGraczObject().SetActive(true);

    }
    private void StartStage(int stage)
    {
        this.stage = stage;
        stageText.SetText("STAGE: " + stage.ToString());
        for (int i = 0; i < stage * 4; i++)
        {
            Object type;
            string typ = "Easy";
            if (gracz.getDiff() == 0)
            {
                typ = "Easy";
                type = (PrzeciwnikEasy)przeciwnikFactory.getPrzeciwnik(typ);
            }
            else if (gracz.getDiff() == 1)
            {
                typ = "Medium";
                type = (PrzeciwnikMedium)przeciwnikFactory.getPrzeciwnik(typ);
            }
            else
            {
                typ = "Hard";
                type = (PrzeciwnikHard)przeciwnikFactory.getPrzeciwnik(typ);
            }
            if (gracz.getGraczObject() == null) return;//kiedy gracz umrze 


            GameObject enemy = Object.Instantiate(przeciwnikFactory.getPrzeciwnik(typ).getPrefab()
           , randomSpawn()
           , Quaternion.identity);
            updateMoney();
            checkUpgradeAvailable();
            enemy.GetComponent<Typ>().setTyp(type);
        }
        for (int i = 0; i < 3; i++)
        {

            Object.Instantiate((GameObject)Resources.Load("Prefabs/DMGBuff")
       , randomSpawn()
       , Quaternion.identity);


            Object.Instantiate((GameObject)Resources.Load("Prefabs/HealBuff")
           , randomSpawn()
           , Quaternion.identity);


            Object.Instantiate((GameObject)Resources.Load("Prefabs/SpeedBuff")
           , randomSpawn()
           , Quaternion.identity);

        }

    }
    private Vector3 randomSpawn()
    {
        Vector3 spawn3 = new Vector3(gracz.getGraczObject().transform.position.x + Random.Range(-32, 32), gracz.getGraczObject().transform.position.y + Random.Range(-32, 32), 0);

        var hitColliders = Physics.OverlapSphere(spawn3, 2);
        while (hitColliders.Length > 0)
        {
            spawn3 = new Vector3(gracz.getGraczObject().transform.position.x + Random.Range(-32, 32), gracz.getGraczObject().transform.position.y + Random.Range(-32, 32), 0);
            hitColliders = Physics.OverlapSphere(spawn3, 2);
        }
        return spawn3;
    }

    private void updateMoney()
    {
        money_ui.text = money.ToString();
    }
    public void checkUpgradeAvailable()
    {
        if (money >= stage * 10)
        {
            if (button_upgrade.GetComponent<Button>().interactable != true)
            {
                button_upgrade.GetComponent<Button>().interactable = true;
            }

        }
        else if (button_upgrade.GetComponent<Button>().interactable != false)
        {
            button_upgrade.GetComponent<Button>().interactable = false;
        }
    }
    public void buyUpgrade()
    {
        gracz.upgrade(5 * stage);
        money -= stage * 10;
        updateMoney();
        checkUpgradeAvailable();

    }
    public void RestartGame()
    {
        panel.SetActive(false);
        Debug.Log("BUTTON");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }
    // Update is called once per frame
    void LateUpdate()
    {

        if (gracz.getHP() <= 0)
        {
            panel.SetActive(true);
            gracz.getGraczObject().SetActive(false);


        }
        if (GameObject.FindGameObjectsWithTag("Enemy").Length < 4)
        {
            money += stage * 20;
            StartStage(++stage);
            updateMoney();
            checkUpgradeAvailable();
        }
    }
}
