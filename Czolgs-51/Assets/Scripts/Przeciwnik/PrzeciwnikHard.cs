﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrzeciwnikHard : MonoBehaviour, IPrzeciwnik
{
    private float color
    , hp
    , speed
    , damage;
    GameObject prefab;
    public GameObject cannonObject;

    public GameObject getCannon()
    {
    
        return cannonObject;
    }

    void Start()
    {
        hp = 50;
        speed = 7;
        damage = 15;
    }
    public float getDamage()
    {
        return damage;
    }
    public float getHP()
    {
        return hp;

    }
    public bool dealDamage(float x)
    {
        this.hp -= x;
        if (hp > 0) return true;
        return false;
    }
    public GameObject getPrefab()
    {
        return prefab;
    }

    public void setPrefab(GameObject o)
    {
        prefab = o;
    }
}