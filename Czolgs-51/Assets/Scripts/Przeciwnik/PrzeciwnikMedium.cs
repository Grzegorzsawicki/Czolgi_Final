﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrzeciwnikMedium : MonoBehaviour, IPrzeciwnik
{
    [SerializeField]
    public float color
    , hp=50
    , speed=8
    , damage=25;
    GameObject prefab;
    public GameObject cannonObject;

    public GameObject getCannon()
    {
        return cannonObject;
    }

    public void setCannon(GameObject o)
    {
        cannonObject = o;
    }
    void Start()
    {

    }
    public float getDamage()
    {
        return damage;
    }
    public float getHP()
    {
        return hp;

    }
    public bool dealDamage(float x)
    {
        this.hp -= x;
        if (hp > 0) return true;
        return false;
    }

    public GameObject getPrefab()
    {
        return prefab;
    }

    public void setPrefab(GameObject o)
    {
        prefab = o;
    }

}
