﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrzeciwnikEasy : MonoBehaviour,IPrzeciwnik
{
    private float color
    , hp
    , speed
    , damage;
    GameObject prefab; public GameObject cannonObject;

    public GameObject getCannon()
    {
        return cannonObject;
    }

    public void setCannon(GameObject o)
    {
        cannonObject = o;
    }
    void Start()
    {
        hp = 25;
        speed = 5;
        damage = 10;
    }
    public float getDamage()
    {
        return damage;
    }
    public float getHP()
    {
        return hp;

    }
    public bool dealDamage(float x)
    {
        this.hp -= x;
        if (hp > 0) return true;
        return false;
    }
    public GameObject getPrefab()
    {
        return prefab;
    }

    public void setPrefab(GameObject o)
    {
        prefab = o;
    }
}
