﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrzeciwnikFactory : MonoBehaviour
{
    Dictionary<string, IPrzeciwnik> przeciwnicy = new Dictionary<string, IPrzeciwnik>();
    public int ObjectCount()
    {
        return przeciwnicy.Count;
    }
    public IPrzeciwnik getPrzeciwnik(string name)
    {
        IPrzeciwnik przeciwnik;
        if (przeciwnicy.ContainsKey(name))
        {
            Debug.Log("\nSpawn: "+name+"\n");
            return przeciwnicy[name];
        }
        else
        {
            if (name == "Easy")
            {
                przeciwnik = new PrzeciwnikEasy();
                przeciwnik.setPrefab((GameObject)Resources
            .Load("Prefabs/PrzeciwnikEasy"));
                
                przeciwnicy.Add("Easy", przeciwnik);
            }
            else if (name == "Medium")
            {
                przeciwnik = new PrzeciwnikMedium();
                przeciwnik.setPrefab((GameObject)Resources
            .Load("Prefabs/PrzeciwnikMedium"));
               
                przeciwnicy.Add("Medium", przeciwnik);
            }
            else if (name == "Hard")
            {
                przeciwnik = new PrzeciwnikHard();
                przeciwnik.setPrefab((GameObject)Resources
            .Load("Prefabs/PrzeciwnikHard"));
                
                przeciwnicy.Add("Hard", przeciwnik);
            }
            else
            {
                przeciwnik= null;
                //nie ma takiego przeciwnika
            }
        }
        return przeciwnik;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
