﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Typ : MonoBehaviour
{
    // Start is called before the first frame update
    Object typ;
    public float period = 2f;
    private float nextActionTime = 0.0f;
    public GameObject kEnemy1;
    public GameObject kEnemy2;
    public GameObject stateEnemy1, stateEnemy2;
    bool enemyBool = false;
    Gracz gracz;
    public void OnDestroy()
    {
        Destroy(stateEnemy1);
        Destroy(stateEnemy2);
        Destroy(kEnemy1);
        Destroy(kEnemy2);
    }
    public Object getTyp()
    {
        return typ;
    }
    public void setTyp(Object t)
    {
        typ = t;
    }
    void Start()
    {
        kEnemy1 = gameObject.transform.Find("Kolo1").gameObject;
        kEnemy2 = gameObject.transform.Find("Kolo2").gameObject;
        kEnemy1.tag = "Enemy";
        kEnemy2.tag = "Enemy";
        gracz = Gracz.getGracz();

        stateEnemy1 = GameObject.Instantiate((GameObject)Resources
            .Load("Prefabs/Move1")
            , new Vector3(0, 0, -10)
            , Quaternion.identity);

        stateEnemy2 = GameObject.Instantiate((GameObject)Resources
            .Load("Prefabs/Move2")
            , new Vector3(0, 0, -10)
            , Quaternion.identity);
        kEnemy1.GetComponent<SpriteRenderer>().sprite = stateEnemy1.GetComponent<SpriteRenderer>().sprite;
        kEnemy2.GetComponent<SpriteRenderer>().sprite = stateEnemy2.GetComponent<SpriteRenderer>().sprite;
        StartCoroutine(shoot());
    }
     private IEnumerator shoot()
    {
        BulletFactory.Shot(GetComponent<IPrzeciwnik>().getCannon());
        yield return new WaitForSeconds(2); //wait 10 seconds
        StartCoroutine(shoot());
    }
    // Update is called once per frame
    void Update()
    {
        if (gracz.getGraczObject() == null) return;

        Vector3 targetPos = gracz.getGraczObject().transform.position;
        Vector3 dir = targetPos - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);

        transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), targetPos, 3 * Time.deltaTime);
       
       
        if (enemyBool == true)
        {
            kEnemy1.GetComponent<SpriteRenderer>().sprite = stateEnemy1.GetComponent<SpriteRenderer>().sprite;
            kEnemy2.GetComponent<SpriteRenderer>().sprite = stateEnemy1.GetComponent<SpriteRenderer>().sprite;

        }
        else
        {
            kEnemy1.GetComponent<SpriteRenderer>().sprite = stateEnemy2.GetComponent<SpriteRenderer>().sprite;
            kEnemy2.GetComponent<SpriteRenderer>().sprite = stateEnemy2.GetComponent<SpriteRenderer>().sprite;
        }
        enemyBool = !enemyBool;
    }
}
