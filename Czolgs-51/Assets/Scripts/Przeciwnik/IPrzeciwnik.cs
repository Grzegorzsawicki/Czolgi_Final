﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPrzeciwnik 
{
    float getDamage();
    float getHP();
    bool dealDamage(float x);
    GameObject getPrefab();
    GameObject getCannon();
    void setPrefab(GameObject o);
}
