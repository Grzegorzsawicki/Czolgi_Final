﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float MaxSpeed;
    public GameObject cannonObject;
    public Rigidbody2D rb;
    Camera cam;
    Vector2 mausePos,movement ;
    public Vector3 previus,previusY;
    Rigidbody2D rbCannon;
    public GameObject k1;
    public GameObject k2;
    public GameObject state1, state2;
    public bool i = true;
    public bool isMoving;
    public float speedThreshold = 0.1f;
    private Gracz gracz;

    public MoveAnimationState moveAnimationState = new MoveAnimationState();
    public IdleAnimationState idleState = new IdleAnimationState();
    private IState currentState;

    private void OnEnable()
    {
        currentState = moveAnimationState;
    }

    void Start()
    {
        gracz = Gracz.getGracz();
        rb = GetComponent<Rigidbody2D>();

        cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        cannonObject = Instantiate((GameObject)Resources
            .Load("Prefabs/"+ gracz.getCannonType())
            , gameObject.transform.position, gameObject.transform.rotation);
        rbCannon = cannonObject.GetComponent<Rigidbody2D>();

        k1 = GameObject.Find("Kolo1");
        k2 = GameObject.Find("Kolo2");
        state1 = GameObject.Instantiate((GameObject)Resources
            .Load("Prefabs/Move1")
            , new Vector3(0, 0, -10)
            , Quaternion.identity);

        state2 = GameObject.Instantiate((GameObject)Resources
            .Load("Prefabs/Move2")
            , new Vector3(0, 0, -10)
            , Quaternion.identity);




        k1.GetComponent<SpriteRenderer>().sprite = state1.GetComponent<SpriteRenderer>().sprite;
        k2.GetComponent<SpriteRenderer>().sprite = state1.GetComponent<SpriteRenderer>().sprite;

    }

    void cannonMove()
    {
        Vector2 look = mausePos - rb.position;
        float angle = Mathf.Atan2(look.y, look.x) * Mathf.Rad2Deg - 90f;
        rbCannon.rotation = angle;
        cannonObject.transform.position = rb.transform.position;
    }
    void playerMove()
    {
    
        isMoving = true;
       

        rb.MovePosition(rb.position + movement * MaxSpeed * Time.fixedDeltaTime);
        //if (movement.y != 0 || movement.x != 0)//fix
            rb.rotation = Mathf.Rad2Deg * Mathf.Atan2(movement.y, movement.x) - 90f;
        //else rb.velocity =Vector2.zero;


    }
    void Update()
    {  currentState = currentState.DoState(this);
        MaxSpeed = gracz.getSpeed();
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        mausePos = cam.ScreenToWorldPoint(Input.mousePosition);

        playerMove();
        cannonMove();
        previus = transform.position;

    }

    public void OnStart()
    {

    }
}
