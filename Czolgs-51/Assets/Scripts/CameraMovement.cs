﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    private Transform target;
    public float smoothness;
    public Vector3 offset;
    private GameObject temp;

    // Start is called before the first frame update
    void Start()
    {
        temp = new GameObject("Camera target");
    }

    void LateUpdate()
    {
        if (GameplayManager.gameStated&& GameObject.FindGameObjectWithTag("PlayerTag")!=null)
        {
            target = GameObject.FindGameObjectWithTag("PlayerTag").transform;
            transform.position = Vector3.Lerp(transform.position, target.position + offset, smoothness);
            transform.LookAt(target);
        }
        else
        {
            target = temp.transform;
            transform.position = Vector3.Lerp(transform.position, target.position + offset, smoothness);
            transform.LookAt(temp.transform);
        }


    }
}
